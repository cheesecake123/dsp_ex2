#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);
	connect(ui->plotButton, SIGNAL(clicked()), this, SLOT(plotButton()));
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}

double sine(double x, double A, double f)
{
	return A*sin(2.0 * M_PI * f * x + 0.0);
}

void DSPMainWindow::plotButton()
{
	int carrierFreq = ui->carrierFreqInput->text().toDouble() * 1000;
	int triangleFreq = carrierFreq / 100;

	QVector<double> xAxis, yAxis;

	for (double i = 0; i <= 44100; i++)
	{
		double x = i / 44100;
		xAxis.push_back(x);
		double c = sine(x, 0.8, carrierFreq);
		double m = triangleFreq * (x - floor(2 * triangleFreq * x + 0.5) / (2 * triangleFreq) ) 
					* pow(-1, floor(2 * triangleFreq * x + 0.5));
		yAxis.push_back( (1 + m) * c ); 
	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->xAxis->setRange(0, 0.01);
	ui->plot->yAxis->setRange(-1, 1);
	ui->plot->replot();
}
