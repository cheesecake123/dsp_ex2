#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPMainWindow(QWidget *parent = 0);
    ~DSPMainWindow();

private slots:
	void plotButton();

private:
    Ui::DSPMainWindow *ui;
};

#endif // DSPMAINWINDOW_H
